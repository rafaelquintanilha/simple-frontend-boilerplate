export function setGreetingText(text) {	
	return { 
		type: "SET_GREETING_TEXT", 
		text 
	};
}